import logging

from Ticker.Ticker import Ticker
import yfinance as yf

class YahooFin(Ticker):
    def __init__(self, tickerName):
        self.TickerName = tickerName
        self.stock = yf.Ticker(self.TickerName)
        self.Price = self.stock.info['regularMarketPrice']
        self.regularMarketDayHigh = self.stock.info['regularMarketDayHigh']


    def getPrice(self):
        logging.info("%s price: %s" %(self.TickerName, self.Price))
        return self.Price

    def getHigh(self):
        logging.info("%s regularMarketDayHigh: %s" %(self.TickerName, self.regularMarketDayHigh))
        return self.regularMarketDayHigh

