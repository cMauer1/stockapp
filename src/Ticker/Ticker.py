from abc import ABCMeta
from abc import abstractmethod

class Ticker(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self, TickerName):
        pass

    @abstractmethod
    def getPrice(self):
        pass
