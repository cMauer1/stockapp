#! /usr/bin/env python3
import argparse
import logging
from Ticker.YahooFin import YahooFin

def args():
    parser = argparse.ArgumentParser(description='Run the stock app')
    parser.add_argument('-v','--verbose', action="store_true",
                        help='verbose logging')
    arguments = parser.parse_args()

    logLevel = logging.INFO
    if arguments.verbose:
        logLevel = logging.DEBUG
    logging.basicConfig(format='%(asctime)s, %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
        datefmt='%Y-%m-%d:%H:%M:%S', level=logLevel)

    return arguments

def main(arguments):
    logging.info("Hello World!")
    stock = YahooFin("GOOG")
    stock.getPrice()
    stock.getHigh()


if __name__ == "__main__":
    arguments = args()
    main(arguments)
